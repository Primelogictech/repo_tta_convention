<?php include 'header.php';?>

    <section class="container-fluid mt35 mt-lg-80">
        <div class="mx-auto main-heading">
            <span>Advisory Council</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10">
                    <div class="row pt-3 pb-5">
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div id="myelement">
                                <img src="images/mallareddy.png" class="img-fluid mx-auto d-block" width="130" alt="" />
                            </div>
                            <div class="pt-3 px-2 text-center">
                                <h6 class="text-violet font-weight-bold">Dr. Pailla Malla Reddy</h6>
                                <div>FOUNDER</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div id="myelement">
                                <img src="images/vijay.jpg" class="img-fluid mx-auto d-block" width="130" alt="" />
                            </div>
                            <div class="pt-3 px-2 text-center">
                                <h6 class="text-violet font-weight-bold">Dr. Vijayapal Reddy</h6>
                                <div>CHAIR</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div id="myelement">
                                <img src="images/haranath_policherla.png" class="img-fluid mx-auto d-block" width="130" alt="" />
                            </div>
                            <div class="pt-3 px-2 text-center">
                                <h6 class="text-violet font-weight-bold">Dr. Haranath Policherla</h6>
                                <div>CO-CHAIR</div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                            <div id="myelement">
                                <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid mx-auto d-block" width="130" alt="" />
                            </div>
                            <div class="pt-3 px-2 text-center">
                                <h6 class="text-violet font-weight-bold">Dr. Mohan Reddy Patalolla</h6>
                                <div>COUNCIL CHAIR</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?>