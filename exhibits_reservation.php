<?php include 'header.php';?>

<style type="text/css">
    input[type="checkbox"], input[type="radio"] {
        box-sizing: border-box;
        padding: 0;
        position: relative;
        top: 2px;
    }
    .table th {
        vertical-align: middle !important; 
    }
    </style>

<section class="container-fluid my-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-md-none shadow-small py-0 pt-1 px-1 pb-md-5">
                <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                            <img src="images/convention-registration.jpeg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row px-3 px-lg-0">
                    <div class="col-12">
                        <h4 class="text-violet text-center mb-3">Exhibits Registration</h4>
                    </div>
                </div>
                <form>
                    <div class="col-12 col-md-12 col-lg-10 offset-lg-1 shadow-small p-3 py-md-5 px-md-4 my-3">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Exhibitor Name:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Exhibitor Name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Exhibitor Type:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Exhibitor Type" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>First Name:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="First Name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Last Name:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Last Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>City:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="City" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>State:</label><span class="text-red">*</span>
                                        <div>
                                            <select class="form-control">
                                                <option>Select State</option>
                                                <option>Ohio</option>
                                                <option>Florida</option>
                                                <option>Texas</option>
                                                <option>Columbia</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Zip Code:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Zip Code" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Country:</label><span class="text-red">*</span>
                                        <div>
                                            <select class="form-control">
                                                <option>Select Country</option>
                                                <option>USA</option>
                                                <option>India</option>
                                                <option>Dubai</option>
                                                <option>Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Phone:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Phone" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Email:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Email" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-3 col-lg-2 my-1 my-md-auto"><label>Address:</label><span class="text-red"> *</span></div>
                                    <div class="form-group col-12 col-md-9 col-lg-10 my-1 my-md-auto">
                                        <div>
                                            <input type="text" name="" class="form-control" placeholder="Address" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row py-5">
                            <div class="col-12">
                                <h5 class="text-violet text-center mb-3">Exhibits Selection</h5>
                            </div>
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="datatable table table-bordered table-center mb-0">
                                        <thead>
                                            <tr>
                                                <th>Package Code</th>
                                                <th>Exhibitor Type</th>
                                                <th>No. of booths</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>EXE01</td>
                                                <td>Jewelry-Gold & Diamond (10’X20’)</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>EXE02</td>
                                                <td>Real Estate & Fin. Services (10’X10’)</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>EXE03</td>
                                                <td>Semi-Precious Jewelry, Sarees &Boutique, Arts & Crafts (10’X10’)</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>EXE04</td>
                                                <td>All other Types (10’X10’)</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>EXE05</td>
                                                <td>Non- Profit (10’X10’)</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-right">Total Amount: $</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row pb-5">
                            <div class="col-12">
                                <h5 class="text-violet text-center mb-3">Digital/Web/Souv.Selection</h5>
                            </div>
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="datatable table table-bordered table-center mb-0">
                                        <thead>
                                            <tr>
                                                <th>Package Code</th>
                                                <th>Description</th>
                                                <th>Qty</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>DIG01</td>
                                                <td>Digital Ads</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>SOU01</td>
                                                <td>Souvenir Ad Full Page</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>SOU02</td>
                                                <td>Souvenir Ad Half Page</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>SOU03</td>
                                                <td>Souvenir Ad Quarter Page</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-right">Total Amount: $</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-12 pt-4">
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="text-violet text-center">Payment Details:</h4>
                                    </div>
                                </div>
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Credit Card Details:</h5>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Card Holder's Name:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Card Holder's Name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Credit Card Number:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Credit Card Number" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Expiration Date:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Expiration Date" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>CVV No:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="CVV No" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-3 col-lg-2 my-auto">
                                         <label>Card Type:</label><span class="text-red"> *</span>
                                    </div>
                                    <div class="form-group col-12 col-md-9 col-lg-10 my-auto">
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/master-card_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/visa-card_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/american-express_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">Other
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-4">
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Billing Address:</h5>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Billing Name (First, Last):</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Billing Name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>City:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="City" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>State:</label><span class="text-red">*</span>
                                        <div>
                                            <select class="form-control">
                                                <option>Select State</option>
                                                <option>Ohio</option>
                                                <option>Florida</option>
                                                <option>Texas</option>
                                                <option>Columbia</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Zip Code:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Zip Code" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Address Line1:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Address Line1" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Address Line2:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Address Line2" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row py-5">
                            <div class="col-12">
                                <h5 class="text-violet text-center">EXHIBITS/ DIGITAL/SOUVENIR PACKAGE DETAILS:</h5>
                            </div>
                            <div class="col-12">
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Exhibits Package Details:</h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="datatable table table-bordered table-center mb-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Package Code</th>
                                                <th rowspan="2">Exhibitor Type/ Booth Size</th>
                                                <th class="text-center" colspan="2">Package Amount</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Before 03/31/2020</th>
                                                <th>After 03/31/2020</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>EXE01</td>
                                                <td>Jewelry – Gold & Diamond Category ( PrimeSpace*) (10’X20’)</td>
                                                <td>$15,000</td>
                                                <td>$16,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE02</td>
                                                <td>Jewelry – Gold & Diamond Category - (10’X20’)</td>
                                                <td>$10,000</td>
                                                <td>$11,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE03</td>
                                                <td>Real Estate & Fin. Services(Prime Space*)(10’X20’)</td>
                                                <td>$15,000</td>
                                                <td>$16,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE04</td>
                                                <td>Real Estate & Fin. Services (10’X10’)</td>
                                                <td>$5,000</td>
                                                <td>$6,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE05</td>
                                                <td>Semi-Precious Jewelry, Sarees, Boutique, Arts & Crafts ( 10’X10’)</td>
                                                <td>$2500</td>
                                                <td>$3,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE06</td>
                                                <td>All other types (10’X10’)</td>
                                                <td>$2500</td>
                                                <td>$3,000</td>
                                            </tr>
                                            <tr>
                                                <td>EXE07</td>
                                                <td>Non- Profit (10’X10’)</td>
                                                <td>$1,000</td>
                                                <td>$1,000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 mt-4">
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Digital/Web/Souvenir Package Details:</h5>
                                </div>
                                <div class="table-responsive">
                                    <table class="datatable table table-bordered table-center mb-0">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Package Code</th>
                                                <th rowspan="2">Description</th>
                                                <th class="text-center" colspan="2">Package Amount</th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Size</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>DIG01</td>
                                                <td>Digital Ads</td>
                                                <td>10 - 20 sec</td>
                                                <td>$2,000 - $5000</td>
                                            </tr>
                                            <tr>
                                                <td>SOU01</td>
                                                <td>Souvenir Ad (Color)</td>
                                                <td>Full Page</td>
                                                <td>$1,000</td>
                                            </tr>
                                            <tr>
                                                <td>SOU02</td>
                                                <td>Souvenir Ad (Color)</td>
                                                <td>Half Page</td>
                                                <td>$500</td>
                                            </tr>
                                            <tr>
                                                <td>EXE04</td>
                                                <td>Souvenir Ad (Color)</td>
                                                <td>Quarter</td>
                                                <td>$250</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
