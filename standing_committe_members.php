<?php include 'header.php';?>

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
    	<div>
            <span>Standing Committe Members</span>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 pt-3 pb-5">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vani SingiriKonda</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>New York</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Nanda Srirama</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Nithyasri KS</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Bay Area</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Supraja Kesireddy</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Rama Vanama</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>New York</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Bala Reddy Maram</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Bhavana Darur</h6>
                                <div>CULTURAL COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Shiva Reddy</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Naveen</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Thirumal Reddy</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Mallik Bolla</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Rajnikanth Rachakatla</h6>
                                <div>CULTURAL COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Madhu Nambeti</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vinod Rama</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Arun Arkala</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Shiva Immadi</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Prashanth Vaida</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Deepthi Miryala</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Manikyam</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sharath Vemuganti</h6>
                                <div>COMMUNITY SERVICE COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Dr. Sunitha Kanumuri</h6>
                                <div>CME AND SEMINAR COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Dr Raj Reddy Sappati</h6>
                                <div>CME AND SEMINAR COMMITTEE</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Dr Nandini Sunkireddy</h6>
                                <div>CME AND SEMINAR COMMITTEE</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Dr Anil Kumar</h6>
                                <div>CME AND SEMINAR COMMITTEE</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Suresh Reddy Venkannagari</h6>
                                <div>FINANCE COMMITTEE - CHAIR</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Pruthvi Sathiri</h6>
                                <div>FINANCE COMMITTEE - CO-CHAIR</div>
                                <div>Los Angeles</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Viju Chiluveru</h6>
                                <div>LANGUAGE AND LITERACY(L&L)COMMITTEE - CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Roopak Kalluri</h6>
                                <div>MEMBERSHIP COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Pradeep Boddu</h6>
                                <div>MEMBERSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Los Angeles</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Goutham Reddy Mulamalla</h6>
                                <div>MEMBERSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Madhusudan Nallu</h6>
                                <div>MEMBERSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Prasad Kunarapu</h6>
                                <div>TTA FOUNDATION COMMITTEE - CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sreedhar Raju</h6>
                                <div>TTA FOUNDATION COMMITTEE - CHAIR</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sanjay Agraharam</h6>
                                <div>NOMINATION/ELECTION COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sudhakar Uppala</h6>
                                <div>TTA JOURNAL COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Arun Arkala</h6>
                                <div>OVERSEAS COORDINATION COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Praneeth Reddy</h6>
                                <div>OVERSEAS COORDINATION COMMITTEE - CO-CHAIR</div>
                                <div>PA-India</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Amar Velmala</h6>
                                <div>OVERSEAS COORDINATION COMMITTEE - CO-CHAIR</div>
                                <div>India</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sohail Ahmed</h6>
                                <div>POLITICAL COMMITTEE - CHAIR</div>
                                <div>Bay Area</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Shanker Bonaili</h6>
                                <div>POLITICAL COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Srinivas Gundeboina</h6>
                                <div>POLITICAL COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Ramana Kotha</h6>
                                <div>PUBLIC RELATIONS COMMITTEE - CHAIR</div>
                                <div>Delaware</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sachin Ghanwar</h6>
                                <div>PUBLIC RELATIONS COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Ramu Sannidhi</h6>
                                <div>SPIRITUAL COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Prabhavathi</h6>
                                <div>SPIRITUAL COMMITTEE - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Satya Reddy Gaggenapally</h6>
                                <div>SPIRITUAL COMMITTEE - CO-CHAIR</div>
                                <div>New York</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vani Gaddam</h6>
                                <div>STUDENTS EXCHANGE COMMITTEE - CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sripal Reddy</h6>
                                <div>STUDENTS EXCHANGE COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Mahesh Sambu</h6>
                                <div>STUDENTS EXCHANGE COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sai Madhav Patel</h6>
                                <div>STUDENTS EXCHANGE COMMITTEE - MEMBER</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Prabhakar Madupathi</h6>
                                <div>SPORTS COMMITTEE - CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Vijay Bhasker Kalal</h6>
                                <div>SPORTS COMMITTEE - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Kiran Gudimalla</h6>
                                <div>SPORTS COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Srikanth Reddy Mogula</h6>
                                <div>SPORTS COMMITTEE - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sangeetha Reddy</h6>
                                <div>WOMEN FORUM COMMITTEE - CHAIR</div>
                                <div>Seattle</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Swathi Chennuri</h6>
                                <div>WOMEN FORUM COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Deepa Jalagam</h6>
                                <div>WOMEN FORUM COMMITTEE - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Jyothsna Kallu</h6>
                                <div>WOMEN FORUM COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Smitha Peddireddy</h6>
                                <div>WOMEN FORUM COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Deepthi Miryala</h6>
                                <div>WOMEN FORUM COMMITTEE - MEMBER</div>
                                <div>Detroit</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Esha Varakur</h6>
                                <div>YOUTH ACTIVITIES COMMITTEE - CO-CHAIR</div>
                                <div>Bay Area</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Vishnu Maddu">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Suresh Kumar Thanda</h6>
                                <div>EDUCATION AND TRAINING - CHAIR</div>
                                <div>Seattle</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Malla Reddy Karra">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Shankar Vulupala</h6>
                                <div>EDUCATION AND TRAINING - CO-CHAIR</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Mohan Patalolla">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Ramesh Punna</h6>
                                <div>EDUCATION AND TRAINING - MEMBER</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suresh Reddy Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Hari Pyreddy</h6>
                                <div>BUSINESS ENTERPRENEURSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venkat Nalluri Kokatam">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Ajay Reddy Macha</h6>
                                <div>BUSINESS ENTERPRENEURSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Seattle</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Bhasker Pinna">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sridhar Reddy Goli</h6>
                                <div>BUSINESS ENTERPRENEURSHIP COMMITTEE - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Nidheesh Raju Baskaruni">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Srikanth Boddireddy</h6>
                                <div>BUSINESS ENTERPRENEURSHIP COMMITTEE - MEMBER</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Venugopal Darur">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Nishanth</h6>
                                <div>SOCIAL MEDIA - CHAIR</div>
                                <div>Charlotte</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Ashok Chintakunta">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Trilok Reddy</h6>
                                <div>SOCIAL MEDIA - CO-CHAIR</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="Suman Mudamba">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Sridhar Chadhu</h6>
                                <div>SOCIAL MEDIA - CO-CHAIR</div>
                                <div>Seattle</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="standing-committe-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="150" alt="">
                            <div class="text-center pt-3 px-2">
                                <h6 class="text-violet font-weight-bold">Swapna Reddy</h6>
                                <div>MATRIMONIAL COMMITTEE - CO-CHAIR</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>