<?php include 'header.php';?>

<section class="container-fluid my-3 my-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-small py-0 pt-1 px-1">
                <div class="row">
                    <div class="col-12">
                        <div>
                            <img src="images/banners/cme.jpg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 p40 px-sm-30">
                        <div>
                            <img src="images/cme.jpeg" class="img-fluid mx-auto d-block p5 img-b2 bg-skyblue" alt="CME" />
                        </div>
                        <div class="text-center fs20 py-3"><i class="fas fa-globe text-info"></i> : <a href="#" class="text-danger text-decoration-underline">www.orchardcorp.com</a></div>
                        <div class="pt-1">
                            <img src="images/cme-flyer.jpeg" class="img-fluid mx-auto d-block" alt="CME Flyer" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
