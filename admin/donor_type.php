<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Donor type</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="add_donor_type.php" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">

		<!-- Recent Orders -->
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Sl NO.</th>
								<th>Donor Type</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Sapphire</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Diamond</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Platinum</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Gold</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>Silver</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td>Bronze</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
							<tr>
								<td>7</td>
								<td>Patron</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /Recent Orders -->

	</div>
</div>

<?php include 'footer.php';?>