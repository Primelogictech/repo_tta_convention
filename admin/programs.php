<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Programs</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="add_programs.php" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Sl NO.</th>
								<th>Chair Name</th>
								<th>Mobile Number</th>
								<th>Co-Chair Name</th>
								<th>Program Name</th>
								<th>Date</th>
								<th>Location</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Prashanth Kaparthi</td>
								<td>313-590-7315</td>
								<td>Swapna Ellendula</td>
								<td>Lakshmi Narasimha Swamy Kalyanam</td>
								<td>
									Jul 5th to 7th, 2021
								</td>
								<td>Suburban Collection Showplace</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">View</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'footer.php';?>