<?php include 'header.php';?>

<style type="text/css">

.checbox1{
	position: absolute;
    top: 37px;
}

.multiselect {
  width: 100%;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: normal;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes1 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes1 label {
  display: block;
}

#checkboxes1 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes2 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes2 label {
  display: block;
}

#checkboxes2 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes3 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes3 label {
  display: block;
}

#checkboxes3 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes4 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes4 label {
  display: block;
}

#checkboxes4 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes5 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes5 label {
  display: block;
}

#checkboxes5 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes6 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes6 label {
  display: block;
}

#checkboxes6 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

#checkboxes7 {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes7 label {
  display: block;
}

#checkboxes7 label:hover {
  background-color: #1e90ff;
  color: #fff;
}

.plus-iconn{
	border: 1px solid;
    padding: 7px;
    border-radius: 20px;
}
</style>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Assign roles</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="leadership.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div class="form-group row">
						<div class="col-12 pt-2">
							<div class="row">
								<div class="col-12 col-lg-3">
									<h6 class="mb-3 font-weight-bold">Type</h6>
								</div>
								<div class="col-12 col-lg-4">
									<h6 class="mb-3 font-weight-bold">Sub-Type</h6>
								</div>
								<div class="col-12 col-lg-4">
									<h6 class="mb-3 font-weight-bold">Designation</h6>
								</div>
							</div>
							<div class="row my-2">
								<div class="col-12 col-sm-6 col-md-3 my-1">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Success Leadership</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />Convention Leadership
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Convention Committee
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Executive Committee
								        </label>
								        <label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Founders & BOD'S
								        </label>
								    </div>
								</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes1()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes1">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />President
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Chairman
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Convenor
								        </label>
								        <label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Co-Convenor
								        </label>
								    </div>
								</div>
								</div>
								<div class="col-lg-1 my-auto">
									<div class="">
										<i class="fas fa-plus plus-iconn"></i>
									</div>
								</div>
							</div>
							<div class="row my-2">
								<!-- <div class="col-12">
									<h6 class="mb-2">Sub-Type</h6>
								</div> -->
								<div class="col-12 col-sm-6 col-md-3 my-1">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Convention Invitees</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes2()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes2">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />Dignitaries
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Entertainment
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Speakers
								        </label>
								    </div>
								</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes3()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes3">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />President
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Chairman
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Convenor
								        </label>
								        <label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Co-Convenor
								        </label>
								    </div>
								</div>
								</div>
							</div>
							<div class="row my-2">
								<div class="col-12 col-sm-6 col-md-3 my-1">
									<input type="checkbox" class="checbox cursor-pointer" name=""><span class="pl25">Convention Donors</span>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes4()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes4">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />Sapphire
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Diamond
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Platinum
								        </label>
								        <label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Gold
								        </label>
								        <label class="py-2 mb-0" for="four">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Silver
								        </label>
								        <label class="py-2 mb-0" for="five">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Bronze
								        </label>
								        <label class="py-2 mb-0" for="sic">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Patron
								        </label>
								    </div>
								</div>
								</div>
								<div class="col-12 col-sm-6 col-md-4 my-1">
									<div class="multiselect w-100">
								    	<div class="selectBox" onclick="showCheckboxes5()">
								      	<select class="form-control">
								        	<option class="font-weight-normal">-- Select --</option>
								      	</select>
								      	<div class="overSelect"></div>
								    </div>
								    <div id="checkboxes5">
								      	<label class="py-2 mb-0" for="one">
								        	<input type="checkbox" class="mx-2" id="one" style="position: relative;top: 2px;" />President
								        </label>
								      	<label class="py-2 mb-0" for="two">
								        	<input type="checkbox" class="mx-2" id="two" style="position: relative;top: 2px;" />Chairman
								        </label>
								      	<label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="three" style="position: relative;top: 2px;" />Convenor
								        </label>
								        <label class="py-2 mb-0" for="three">
								        	<input type="checkbox" class="mx-2" id="four" style="position: relative;top: 2px;" />Co-Convenor
								        </label>
								    </div>
								</div>
								</div>
							</div>
						</div>
					</div>

					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script type="text/javascript">
	var expanded = false;

	function showCheckboxes() {
	  	var checkboxes = document.getElementById("checkboxes");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes1() {
	  	var checkboxes = document.getElementById("checkboxes1");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes2() {
	  	var checkboxes = document.getElementById("checkboxes2");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes3() {
	  	var checkboxes = document.getElementById("checkboxes3");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes4() {
	  	var checkboxes = document.getElementById("checkboxes4");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes5() {
	  	var checkboxes = document.getElementById("checkboxes5");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes6() {
	  	var checkboxes = document.getElementById("checkboxes6");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}

	var expanded = false;

	function showCheckboxes7() {
	  	var checkboxes = document.getElementById("checkboxes7");
	  		if (!expanded) {
	    	checkboxes.style.display = "block";
	    	expanded = true;
	  	} else {
	    	checkboxes.style.display = "none";
	    	expanded = false;
	  	}
	}
</script>

<script>
	CKEDITOR.replace( 'editor1' );
</script>

<?php include 'footer.php';?>