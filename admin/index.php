<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="page-title">Welcome Admin!</h3>
            <ul class="breadcrumb">
                <li class="breadcrumb-item active">Dashboard</li>
            </ul>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/users-icon.png" class="img-fluid users-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>168</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Registrations</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/sellers-icon.png" class="img-fluid sellers-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>487</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Invitees</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-success w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/property-icon.png" class="img-fluid properties-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>485</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Donors</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-danger w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/service-seller-icon.png" class="img-fluid service-seller-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>$62523</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Donations Collected</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-warning w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/service-list-icon.png" class="img-fluid service-list-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>168</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Total Programs</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-blue w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 col-12">
        <div class="card">
            <div class="card-body">
                <div class="dash-widget-header">
                    <span>
                        <img src="images/users-icon.png" class="img-fluid users-icon" alt="" />
                    </span>
                    <div class="dash-count">
                        <h3>168</h3>
                    </div>
                </div>
                <div class="dash-widget-info">
                    <h6 class="text-muted">Total Events</h6>
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary w-50"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php';?>
