<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">View Event Details</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="events_n_schedule.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->
<div class="row">
	<div class="col-md-12">

		<!-- Recent Orders -->
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table-bordered table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>S. No</th>
								<th>Day</th>
								<th>Time</th>
								<th>Program</th>
								<th>Room</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td rowspan="13">Friday, July 5th 2019</td>
								<td>7:30 AM to 9:30 AM</td>
								<td>Breakfast</td>
								<td>DIAMOND ROOM HALLWAY</td>
							</tr>
							<tr>
								<td>2</td>
								<td>9:00 AM to 5:00 PM</td>
								<td>Business conference</td>
								<td>CRYSTAL/SAPPHIRE/RUBY</td>
							</tr>
							<tr>
								<td>3</td>
								<td>9:30 AM to 2:00 PM</td>
								<td>Women’s Conference</td>
								<td>PLATINUM BALL ROOM</td>
							</tr>
							<tr>
								<td>4</td>
								<td>9:00 AM to 2:30 PM</td>
								<td>Parents Meet up (Matrimony)</td>
								<td>ONYX/OPAL/GARNET</td>
							</tr>
							<tr>
								<td>5</td>
								<td>3:00 AM to 7:00 PM</td>
								<td>Young Adults Meet up (Matrimony)</td>
								<td>ONYX/OPAL/GARNET</td>
							</tr>
							<tr>
								<td>6</td>
								<td>11:30 PM to 2:00 PM</td>
								<td>Lunch</td>
								<td>DIAMOND ROOM HALLWAY</td>
							</tr>
							<tr>
								<td>7</td>
								<td>12:30 pm -5:15pm</td>
								<td>CME</td>
								<td>EMERALD/AMETHYST</td>
							</tr>
							<tr>
								<td>8</td>
								<td>3:00 PM to 5:00 PM</td>
								<td>Youth Conference</td>
								<td>PLATINUM BALL ROOM</td>
							</tr>
							<tr>
								<td>9</td>
								<td>6:00 PM to 12:00 AM</td>
								<td>Youth Banquet</td>
								<td>PLATINUM BALL ROOM</td>
							</tr>
							<tr>
								<td>10</td>
								<td>6:00 PM to 7:00 PM</td>
								<td>Meet and Greet</td>
								<td>HALL C</td>
							</tr>
							<tr>
								<td>11</td>
								<td>7:00 PM to 1:00 AM</td>
								<td>Awards Banquet</td>
								<td>HALL A</td>
							</tr>
							<tr>
								<td>12</td>
								<td>6:00 PM to 11:00 PM</td>
								<td>Day Care</td>
								<td>JADE/PEARL/CORAL</td>
							</tr>
							<tr>
								<td>13</td>
								<td>10:00 PM to Midnight</td>
								<td>Late Night Lock-In</td>
								<td>Paradise Park</td>
							</tr>
							<tr>
								<td>14</td>
								<td>Exhibits - Friday, July 5th Saturday, July 6th 2019</td>
								<td>8:00 AM to 11:00 PM</td>
								<td>Exhibits</td>
								<td>HALL B</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<!-- /Recent Orders -->

	</div>
</div>
<?php include 'footer.php';?>