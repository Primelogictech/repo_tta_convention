    <?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
    <div class="row">
        <div class="col-9 col-sm-6 my-auto">
            <h5 class="page-title mb-0">Exhibit Registrations</h5>
        </div>
        <div class="col-3 col-sm-6 col-md-6 my-auto">
            <div class="float-right">
                <a href="#" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<!-- /Page Header -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="datatable table table-hover table-center mb-0">
                        <thead>
                            <tr>
                                <th align="center" rowspan="2">S.No</th>
                                <th align="center" rowspan="2"><strong>Exhibitor Type</strong></th>
                                <th align="center" rowspan="2"><strong>Size</strong></th>
                                <th align="center"><strong>Before</strong></th>
                                <th align="center"><strong>After</strong></th>
                            </tr>
                            <tr>
                                <th align="center">
                                    <strong>4/19/2019</strong>
                                </th>
                                <th align="center">
                                    <strong>4/19/2019</strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="center" rowspan="2">1</td>
                                <td align="left" rowspan="2">Jewelry
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$7,500.00</td>
                                <td align="center">$8,000.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$4,000.00</td>
                                <td align="center">$5,000.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">2</td>
                                <td align="left" rowspan="2">Real Estate/Corporate
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$4,500.00</td>
                                <td align="center">$5,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$3,000.00</td>
                                <td align="center">$4,000.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">3</td>
                                <td align="left" rowspan="2">
                                        Pearls/Gems/<br />
                                        Artificial Jewelry
                                    
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$3,500.00</td>
                                <td align="center">$4,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$2,000.00</td>
                                <td align="center">$3,000.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">4</td>
                                <td align="left" rowspan="2">
                                        Apparel/Clothing<br />
                                        Boutiques
                                    
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$2,500.00</td>
                                <td align="center">$3,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$1,500.00</td>
                                <td align="center">$2,500.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">5</td>
                                <td align="left" rowspan="2">Insurance/Telecom
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$4,500.00</td>
                                <td align="center">$5,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$2,500.00</td>
                                <td align="center">$3,500.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">6</td>
                                <td align="left" rowspan="2">
                                        Financial/Web<br />
                                        Education/Professional
                                    
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$3,500.00</td>
                                <td align="center">$4,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$2,500.00</td>
                                <td align="center">$3,500.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">7</td>
                                <td align="left" rowspan="2">Arts/Crafts
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$2,500.00</td>
                                <td align="center">$3,000.00</td>
                            </tr>
                            <tr>
                                <td align="center">
                                    10’ x 10’
                                </td>
                                <td align="center">
                                    $1,500.00
                                </td>
                                <td align="center">
                                    $2,000.00
                                </td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">8</td>
                                <td align="left" rowspan="2">Booth – General
                                </td>
                                <td align="center">10’ x 20’</td>
                                <td align="center">$2,500.00</td>
                                <td align="center">$3,500.00</td>
                            </tr>
                            <tr>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$1,500.00</td>
                                <td align="center">$2,500.00</td>
                            </tr>
                            <tr>
                                <td align="center" rowspan="2">9</td>
                                <td align="left">Home Based Businesses
                                </td>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$1,500.00</td>
                                <td align="center">$2,000.00</td>
                            </tr>
                            <tr>
                                <td align="left" width="30%">Non-Profit Organizations
                                </td>
                                <td align="center">10’ x 10’</td>
                                <td align="center">$1,500.00</td>
                                <td align="center">$2,000.00</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php';?>
