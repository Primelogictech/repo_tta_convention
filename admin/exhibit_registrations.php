<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Exhibit registrations</h5>
		</div>
		<!-- <div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href=".php" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div> -->
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Sl NO.</th>
								<th>Full Name</th>
                                <th>Comapany Name</th>
                                <th>Category</th>
                                <th>Tax ID</th>
                                <th>Mailing Address</th>
                                <th>Phone No</th>
                                <th>Email Id</th>
                                <th>Fax</th>
                                <th>Name of your Website</th>
                                <th>Booth Type</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Arun Kumar</td>
                                <td>Prime Logic Technologies</td>
                                <td>.........</td>
                                <td>A23BDF</td>
                                <td>.........</td>
                                <td>9052304477</td>
                                <td>arunkumar@gmail.com</td>
                                <td>.........</td>
                                <td>NRIVA Conventions</td>
                                <td>.........</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">Delete</a>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Active</a>
									<a href="#" class="btn btn-sm btn-success my-1 mx-1">In-Active</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>