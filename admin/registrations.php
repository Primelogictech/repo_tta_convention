<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Registrations</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="#" title="" class="add-new-btn btn" data-original-title="Add New"><i class="fa fa-plus"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-body">
				<div class="table-responsive">
					<table class="datatable table table-hover table-center mb-0">
						<thead>
							<tr>
								<th>Sl NO.</th>
								<th>Member ID</th>
								<th>Member Details</th>
								<th>Sponsorship Type</th>
								<th>Sponsorship Category</th>
								<th>Amount Paid</th>
								<th>Payment Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>12345</td>
								<td>
									<div class="py-1">
										<span>Name:</span><span> Arun</span>
									</div>
									<div class="py-1">
										<span>Email:</span><span> arun@gmail.com</span>
									</div>
									<div class="py-1">
										<span>Mobile:</span><span> 9052304477</span>
									</div>
								</td>
								<td>Donor</td>
								<td>Bronze ($3000)</td>
								<td>$3000</td>
								<td>Full amount paid</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="assign_features.php" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>23456</td>
								<td>
									<div class="py-1">
										<span>Name:</span><span> Dinesh</span>
									</div>
									<div class="py-1">
										<span>Email:</span><span> dinesh@gmail.com</span>
									</div>
									<div class="py-1">
										<span>Mobile:</span><span> 9230447709</span>
									</div>
								</td>
								<td>Donor</td>
								<td>Diamond ($5000)</td>
								<td>$5000</td>
								<td>Full amount paid</td>
								<td>
									<a href="#" class="btn btn-sm btn-success text-white my-1 mx-1">Edit</a>
									<a href="assign_features.php" class="btn btn-sm btn-success my-1 mx-1">Assign Features</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'footer.php';?>
