<?php include 'header.php';?>

<!-- Page Header -->

<div class="page-header">
	<div class="row">
		<div class="col-9 col-sm-6 my-auto">
			<h5 class="page-title mb-0">Add Programs</h5>
		</div>
		<div class="col-3 col-sm-6 col-md-6 my-auto">
			<div class="float-right">
				<a href="programs.php" data-toggle="tooltip" title="" class="btn back-btn" data-original-title="Cancel"><i class="fa fa-reply"></i></a>
			</div>
		</div>
	</div>
</div>

<!-- /Page Header -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<form action="" method="post">
					<div>
						<h5 class="mb-3">Contact Details :</h5>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Chair Name<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Chair Name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Mobile Number<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="number" class="form-control" name="" placeholder="Mobile Number">
						</div>
					</div>	
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Co-Chair Name<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Co-Chair Name">
						</div>
					</div>
					<div>
						<h5 class="mb-3">Program Details :</h5>
					</div>			
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Program Name<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Program Name">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Date<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="date" class="form-control" name="" >
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Location<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<input type="text" class="form-control" name="" placeholder="Location">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-form-label col-md-4 my-auto">Create Schedule<span class="mandatory">*</span></label>
						<div class="col-md-8 my-auto">
							<textarea name="editor1"></textarea>
						</div>
					</div>

					<div class="text-right">
						<input class="btn btn-primary" type="submit" name="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>

<script>
	CKEDITOR.replace( 'editor1' );
</script>

<?php include 'footer.php';?>
