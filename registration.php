<?php include 'header.php';?>

<style type="text/css">
    input[type="checkbox"], input[type="radio"] {
        box-sizing: border-box;
        padding: 0;
        position: relative;
        top: 2px;
    }
</style>

<section class="container-fluid my-5">
    <div class="container">
        <div class="row">
            <div class="col-12 shadow-md-none shadow-small py-0 pt-1 px-1 pb-md-5">
                <div class="row">
                    <div class="col-12 pb-5">
                        <div>
                            <img src="images/convention-registration.jpeg" class="img-fluid w-100" alt="" />
                        </div>
                    </div>
                    <div class="col-12 col-lg-10 offset-lg-1 shadow-small p-3 p-md-4">
                        <div class="row">
                            <!-- Personal Information -->

                            <div class="col-12">
                                <h5 class="text-violet py-2">Personal Information</h5>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>First Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Last Name:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse First Name:</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Spouse First Name" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Spouse Last Name:</label>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Spouse Last Name" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- End of Personal Information -->

                            <!-- Contact Information -->

                            <div class="col-12">
                                <h5 class="py-2 text-violet">Contact Information</h5>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Email Id:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Email Id" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Mobile:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Mobile" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Country:</label><span class="text-red">*</span>
                                            <div>
                                                <select class="form-control">
                                                    <option>Select Country</option>
                                                    <option>USA</option>
                                                    <option>India</option>
                                                    <option>Dubai</option>
                                                    <option>Other</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>State:</label><span class="text-red">*</span>
                                            <div>
                                                <select class="form-control">
                                                    <option>Select State</option>
                                                    <option>Ohio</option>
                                                    <option>Florida</option>
                                                    <option>Texas</option>
                                                    <option>Columbia</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>City:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="City" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6">
                                            <label>Zip Code:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Zip Code" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mb15">
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto"><label>Address:</label><span class="text-red"> *</span></div>
                                        <div class="col-12 col-md-9 col-lg-9 my-1 my-md-auto">
                                            <div>
                                                <input type="text" name="" class="form-control" placeholder="Address" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row mb15">
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto"><label class="mb-0">Chidren:</label><span class="text-red"> *</span></div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>below 6 Years</label></div>
                                                <input type="text" name="" class="form-control" placeholder="" />
                                            </div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>Age 7-15</label></div>
                                                <input type="text" name="" class="form-control" placeholder="" />
                                            </div>
                                        <div class="col-12 col-md-3 col-lg-3 my-1 my-md-auto">
                                            <div>
                                                <label>Youth 16-23</label></div>
                                                <input type="text" name="" class="form-control" placeholder="" />
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>

                        <!-- end of Contact Information -->

                        <hr class="dashed-hr" />

                        <div class="row">
                            <div class="col-12 pt-2">
                                <div class="row">
                                    <div class="col-12 col-md-6 col-lg-5">
                                        <h5 class="text-violet">Registration / Donation Category</h5>
                                    </div>
                                    <div class="col-12 col-md-4 col-lg-4 px-3 px-md-0 px-lg-3">
                                        <div><input type="radio" id="family_r_individual" name="category" class="" name="" /><span class="fs16 mx-3">Registration Package</span></div>
                                    </div>
                                    <div class="col-12 col-md-2 col-lg-3 px-3 px-md-0 px-lg-3">
                                        <div><input type="radio" id="donors" name="category" class="" name="" /><span class="fs16 mx-3">Donation Package</span></div>
                                    </div>
                                </div>

                                <!-- Family / Individual Table -->

                                <div class="row my-3">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="datatable table table-bordered table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>S.No</th>
                                                        <th>Registration Category</th>
                                                        <th>Price</th>
                                                        <th style="width: 120px;">Count</th>
                                                        <th>Amount</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>One Person (Age 7 Years and Above)(May 27 & May 29,2022)</td>
                                                        <td class="text-orange font-weight-bold">$ 250.00</td>
                                                        <td>
                                                            <input type="text" class="form-control" name="" />
                                                        </td>
                                                        <td>$ 20,000</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>One Person ( May 28 or May 29,2022)</td>
                                                        <td class="text-orange font-weight-bold">$ 250.00</td>
                                                        <td>
                                                            <input type="text" class="form-control" name="" />
                                                        </td>
                                                        <td>$ 20,000</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- End of Family / Individual Table -->

                                <!-- Donor's Table -->

                                <div class="row my-3">
                                    <div class="col-12">
                                        <div class="table-responsive">
                                            <table class="datatable table table-bordered table-center mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Sponsor Category</th>
                                                        <th>Amount</th>
                                                        <th style="min-width: 300px;">Benefits</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Chakravarti Poshakulu</td>
                                                        <td class="text-orange font-weight-bold">$25000</td>
                                                        <td>
                                                            <div class="col-12 col-md-12 col-lg-12">
                                                                <ul class="flower-list">
                                                                    <li>16 Banquet Tickets</li>
                                                                    <li>16 Event Tickets</li>
                                                                    <li>5 Hotel Rooms</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Maharaja Poshakulu</td>
                                                        <td class="text-orange font-weight-bold">$20000</td>
                                                        <td>
                                                            <div class="col-12 col-md-12 col-lg-12">
                                                                <ul class="flower-list">
                                                                    <li>16 Banquet Tickets</li>
                                                                    <li>16 Event Tickets</li>
                                                                    <li>5 Hotel Rooms</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Raja Poshakulu</td>
                                                        <td class="text-orange font-weight-bold">$15000</td>
                                                        <td>
                                                            <div class="col-12 col-md-12 col-lg-12">
                                                                <ul class="flower-list">
                                                                    <li>1 Vendor booth space</li>
                                                                    <li>2 Hotel Rooms (3 nights)</li>
                                                                    <li>2 Shathamanam Bhavathi</li>
                                                                    <li>Admission Pack (12 Tickets)</li>
                                                                    <li>Banquet Admission (12)</li>
                                                                    <li>Kalyanam Tickets (8)</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Mukhya Atithi</td>
                                                        <td class="text-orange font-weight-bold">$10000</td>
                                                        <td>
                                                            <div class="col-12 col-md-12 col-lg-12">
                                                                <ul class="flower-list">
                                                                    <li>1 Vendor booth space(10x10)</li>
                                                                    <li>2 Hotel Rooms (3 nights)</li>
                                                                    <li>2 Shathamanam Bhavathi</li>
                                                                    <li>Admission Tickets (10)</li>
                                                                    <li>Banquet Tickets (10)</li>
                                                                    <li>6 Kalyanam Tickets</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Visishta Atithi</td>
                                                        <td class="text-orange font-weight-bold">$5,000</td>
                                                        <td>
                                                            <div class="col-12">
                                                                <ul class="flower-list">
                                                                    <li>Banner on the Website</li>
                                                                    <li>8 Admission, Banquet Tickets (8)</li>
                                                                    <li>Kalyanam Tickets (4)</li>
                                                                    <li>1 Hotel Rooms (3 nights)</li>
                                                                    <li>Reserved Seating</li>
                                                                    <li>Half page advertisement in Souvenir</li>
                                                                    <li>Shathamanam Bhavathi ticket (1)</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Pratyeka Atithi</td>
                                                        <td class="text-orange font-weight-bold">$2,500</td>
                                                        <td>
                                                            \
                                                            <div class="col-12">
                                                                <ul class="flower-list">
                                                                    <li>6Admission,6 Banquet Tickets</li>
                                                                    <li>Kalyanam Tickets (2)</li>
                                                                    <li>1 Hotel Rooms (3 nights)</li>
                                                                    <li>Half page advertisement in Souvenir</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <input type="radio" name="sponsor_category" />
                                                            </div>
                                                        </td>
                                                        <td>Gourava Atithi</td>
                                                        <td class="text-orange font-weight-bold">$1,500</td>
                                                        <td>
                                                            <div class="col-12">
                                                                <ul class="flower-list">
                                                                    <li>Admission Tickets (4)</li>
                                                                    <li>Banquet Tickets (2)</li>
                                                                    <li>Kalyanam Tickets (1)</li>
                                                                    <li>1 Hotel Rooms (2 nights)</li>
                                                                    <li>Listing in Souvenir</li>
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <!-- End of Donor's Table -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-violet py-2">Additional DONOR Benefits (Admission to)</h5>
                                <ul class="flower-list ml-3">
                                    <li>Business Conference</li>
                                    <li>Women's Conference</li>
                                    <li>Youth Conference</li>
                                    <li>Youth Late Night Lockin</li>
                                    <li>CME</li>
                                    <li>Matrimonial Admission</li>
                                    <li>Immigration Seminar</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row px-3">
                            <div class="col-12 col-sm-10 col-md-6 col-lg-5 shadow-small py-4">
                                <h6 class="text-center">Upload Photo</h6>
                                <div class="browse-button py-2">
                                    <input type="file" class="form-control browse" name="sponsor_category" />
                                </div>
                                <div class="pt-3 text-center fs15">( File Size could not be more than 5MB )</div>
                            </div>
                        </div>
                        <div class="row my-2 px-3">
                            <div class="col-12 shadow-small p-4">
                                <div>
                                    <div class="text-orange fs16">Note:-</div>
                                    <ul class="list-unstyled pt-2 mb-0">
                                        <li class="py-1">1. All registrations are on first come first serve basis and are final</li>
                                        <li class="pt-1">2. *Copy of ID / Student ID / Visitors Visa must be presented when collecting the registration kit.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 pt-4">
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="text-violet text-center">Payment Details:</h4>
                                    </div>
                                </div>
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Credit Card Details:</h5>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Card Holder's Name:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Card Holder's Name" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Credit Card Number:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Credit Card Number" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Expiration Date:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Expiration Date" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>CVV No:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="CVV No" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-3 col-lg-2 my-auto">
                                         <label>Card Type:</label><span class="text-red"> *</span>
                                    </div>
                                    <div class="form-group col-12 col-md-9 col-lg-10 my-auto">
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/master-card_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/visa-card_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">
                                                        <img src="images/american-express_logo.png" class="img-fluid" alt="Mater Card Logo">
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-3 my-auto">
                                                <div>
                                                    <input type="checkbox" name="">
                                                    <span class="pl-3">Other
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 pt-4">
                                <div class="my-2">
                                    <h5 class="border-bottom d-inline-block">Check/Cash Payment Details:</h5>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Total Amount $:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Total Amount" />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Bank Name:</label><span class="text-red">*</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Bank Name" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Check No.:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Check No." />
                                        </div>
                                    </div>
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Check Date:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Check Date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-12 col-md-6 col-lg-6">
                                        <label>Check Received By:</label><span class="text-red"> *</span>
                                        <div>
                                            <input type="text" name="" id="" class="form-control" placeholder="Check Received By" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-12">
                                <h5 class="text-violet py-2">Payment Types</h5>
                                <div>
                                    <span class="mr-1 mr-md-5 position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">PayPal</span></span>
                                    <span class="position-relative pr-3"><input type="radio" class="p-radio-btn" name="payment_type" /><span class="fs16 pl-4 pl-md-4">Cheque/Cash</span></span>
                                </div>
                            </div>
                        </div>
                        <hr class="dashed-hr" />
                        <div class="row">
                            <div class="col-12">
                                <h5 class="text-violet mb-3">Payment by using Cheque</h5>
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Cheque Number: </label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Cheque Number" />
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 col-lg-6 mb-1">
                                            <label>Handed over to:</label><span class="text-red">*</span>
                                            <div>
                                                <input type="text" name="" id="" class="form-control" placeholder="Handed over to" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <hr class="dashed-hr" />
                        <div class="row">
                            <div class="col-12 col-md-6 col-lg-6 pt-2">
                                <h5>Security Code</h5>
                                <div class="row">
                                    <div class="col-11">
                                        <div class="registration-captcha-block">
                                            <div class="row">
                                                <div class="col-8 px-0 my-auto">
                                                    <input type="text" class="border-0 form-control bg-transparent" name="" placeholder="Enter the Characters you see" />
                                                </div>
                                                <div class="col-4 px-0 my-auto">
                                                    <img src="images/ShowCaptchaImage.png" class="captcha-img" alt="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1 px-2 my-auto">
                                        <div>
                                            <img src="images/refresh.png" class="img-fluid mx-auto d-block" width="15" height="16" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 offset-md-1 col-md-5 pt-2">
                                <div class="row my-1">
                                    <div class="col-12 px-3 px-md-0">
                                        <div class="row">
                                            <div class="col-7 col-md-8 my-auto">
                                                <label class="mb-0">Registration Amount</label>
                                            </div>
                                            <div class="col-1 my-auto">
                                                <label class="mb-0">:</label>
                                            </div>
                                            <div class="col-3 my-auto">
                                                <div class="payment">$ 0</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-1">
                                    <div class="col-12 px-3 px-md-0 border-bottom">
                                        <div class="row mb-2">
                                            <div class="col-7 col-md-8 my-auto">
                                                <label class="mb-0">Donation Amount</label>
                                            </div>
                                            <div class="col-1 my-auto">
                                                <label class="mb-0">:</label>
                                            </div>
                                            <div class="col-3 my-auto">
                                                <div class="payment">$ 0</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row my-1">
                                    <div class="col-12 px-3 px-md-0">
                                        <div class="row mb-2">
                                            <div class="col-7 col-md-8 my-auto">
                                                <label class="mb-0">Total Amount</label>
                                            </div>
                                            <div class="col-1 my-auto">
                                                <label class="mb-0">:</label>
                                            </div>
                                            <div class="col-3 my-auto">
                                                <div class="payment">$ 0</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="dashed-hr" />
                        <div class="row py-2">
                            <div class="col-12 col-sm-6 col-md-6 my-auto">
                                <div><input type="checkbox" class="input-checkbox" name="" /><span class="pl25 fs16">I accept Terms and Conditions</span></div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-6 my-2 my-md-auto">
                                <div class="text-center text-sm-right">
                                    <div class="btn btn-lg btn-danger text-uppercase px-5">Check out</div>
                                </div>
                            </div>
                        </div>
                        <div class="row pt-3 pb-2">
                            <div class="col-12 fs16">
                                <div class="">
                                    <span class="text-uppercase font-weight-bold">MAKE CHECKS PAYABLE TO :</span>
                                    <span>TTA</span>
                                </div>
                                <div>Cheque Mailing Address:-</div>
                                <div>5 Independence Way,</div>
                                <div>Suite 300,</div>
                                <div>Princeton,</div>
                                <div>New Jersey,</div>
                                <div>]08540, US</div>
                                <div>
                                    <span><b>For online registration</b></span>
                                    <span>and Convention programs & updates, please visit</span>
                                    <a href="https://ttaconvention.org/" target="_blank" class="text-orange">ttaconvention.org</a>
                                </div>
                                <div>
                                    <span><b>For Registration Information, email,</b></span>
                                    <a href="" class="text-orange">info@telanganaus.org</a>
                                    <span>or reach</span>
                                    <a href="#" class="text-orange">Registration Committee.</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>
