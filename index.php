﻿<?php include 'header.php';?>

<style type="text/css">
    body {
        background: #f1a62a;
        background-image: url(images/body-bg.png);
        background-repeat: repeat-y;
        background-size: contain;
    }
    .main-heading {
        background: #fbfbfb;
    }
</style>

<!-- Content -->
<div class="container-fluid invitees_bg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h5 class="font-weight-bold mb-2" style="color: #ca1515;">Convention Invitees</h5>
            </div>
            <div class="col-12 col-md-8 offset-md-2 col-lg-7 offset-lg-3">
                <div class="row invitees-scroll">
                    <div class="col-md-4 col-lg-4 px-3">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/suma.kanakala.jpg" class="img-fluid mx-auto d-block border-radius-10" width="80px" alt="" />
                            </div>
                            <div class="my-auto">
                                <div class="fs13">Suma Kanakala</div>
                                <div>Anchor</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 px-3">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/sp.sailaja.jpg" class="img-fluid mx-auto d-block border-radius-10" width="80px" alt="" />
                            </div>
                            <div class="my-auto">
                                <div class="fs13">S.P. Sailaja</div>
                                <div>Singer</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 px-3">
                        <div class="invitees d-flex">
                            <div class="p-2">
                                <img src="images/sp.charan.jpg" class="img-fluid mx-auto d-block border-radius-10" width="80px" alt="" />
                            </div>
                            <div class="my-auto">
                                <div class="fs13">S.P. Charan</div>
                                <div>Singer</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Carousel Banners -->

<section class="container-fluid my-0">
    <div class="row">
        <div class="col-12 pt2 pb-0 px-0">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100 my-auto" src="images/banner1.jpeg" alt="First slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/banner1.jpeg" alt="Second slide" />
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 my-auto" src="images/banner1.jpeg" alt="Third slide" />
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- End of Carousel Banners -->
<section class="container-fluid bg-white" style="margin-top: -16px;">
    <div class="row px-1">
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-date-before-triangle">
            <a href="calender.php" class="text-decoration-none">
                <div class="schedule-date-bg">
                    <div class="schedule-date-bg-dots-img">
                        <div class="icon1">
                            <div class="py-5 py-lg-75 text-center">
                                <span class="fs25 text-white">May 27<sup>th</sup> to 29<sup>th</sup>, 2022<br /></span>
                            </div>
                            <div></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-12 col-lg-6 my-1 my-lg-auto px-2 location-before-triangle">
            <div class="schedule-location-bg">
                <div class="location-bg-dots-img">
                    <div class="icon2">
                        <div class="py-5 py-lg-63point5">
                            <h4 class="text-white text-left text-center fs23">New Jersey Convention And Exposition Center</h4>
                            <div class="text-left text-white text-center fs16">97 Sunfield Ave, Edison, NJ 08837, United States</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-3 my-1 my-lg-auto px-2 schedule-register-before-triangle">
            <div class="schedule-register-bg">
                <div class="schedule-date-bg-dots-img">
                    <div class="text-center text-lg-right text-xl-center py-5 py-lg-70">
                        <a href="registration.php" class="btn register-today-btn">REGISTER TODAY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Event Schedule -->

<!-- Convention Messages -->

<section class="container-fluid pt20 pb80 bg-white">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-12 py-3">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="president-message mb-3 text-center">President Message</h6>
                    <div class="shadow-small border-radius-10 president-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="images/Mohan-Patalolla.jpg" class="img-fluid rounded-circle" width="110px" height="110px" alt="Mohan Patalolla" style="border: 3px solid #f9a565;" />
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Mohan Reddy Patalolla</div>
                                <div class="text-center text-white fs14">President</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                With great privilege and honor, I assume the role of President of the Telangana American Telugu Association for the next two years....
                            </p>
                            <div class="text-center pt-3">
                                <a href="president_message_details.php">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="president-elect-message mb-3 text-center">President Elect Message</h6>
                    <div class="shadow-small border-radius-10 president-elect-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="images/vamshi_Reddy_2.jpg" class="img-fluid mx-auto d-block rounded-circle" width="110px" height="110px" alt="Vamshi Reddy" style="border: 3px solid #f9a565;" />
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Vamshi Reddy</div>
                                <div class="text-center text-white fs14">President Elect</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                With great privilege and honor, I assume the role of President Elect of the Telangana American Telugu Association for the next two years....
                            </p>
                            <div class="text-center pt-3">
                                <a href="president_elect_details.php">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                    <h6 class="convenor-message mb-3 text-center">Convenor Message</h6>
                    <div class="shadow-small border-radius-10 convenor-msg_bg">
                        <div class="msg_flower_bg d-flex">
                            <div class="p-3 my-auto">
                                <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid mx-auto d-block rounded-circle" width="110px" height="110px" alt="Srinivas Ganagoni" style="border: 3px solid #f9a565;" />
                            </div>
                            <div class="pt-0 pb-2 text-white my-auto">
                                <div class="text-center mb-1 fs16 font-weight-bold">Srinivas Ganagoni</div>
                                <div class="text-center text-white fs14">Convenor</div>
                            </div>
                        </div>
                        <div class="text-white px-3 pb-4">
                            <p class="pb-2 mb-0 text-center description">
                                With great privilege and honor, I assume the role of Convenor of the Telangana American Telugu Association for the next two years....
                            </p>
                            <div class="text-center pt-3">
                                <a href="president_convenor_details.php">
                                    <i class="fas fa-chevron-circle-right text-white fs20"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Messages -->

<!-- Convention Team -->

<section class="container-fluid pb80">
    <div class="mx-auto main-heading">
        <span>Convention Team</span>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center horizontal-scroll">
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none convention-team_active_tab_btn" target-id="#convention_advisory_committee">Convention&nbsp;Advisory&nbsp;Committee</a>
                        </li>
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_executive_committee">Convention&nbsp;Executive&nbsp;Committee</a>
                        </li>
                        <li class="convention-team-nav-item d-inline-flex">
                            <a class="convention-team-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#convention_regional_advisors">Convention&nbsp;Regional&nbsp;Advisors</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center convention-team_active_tab" id="convention_advisory_committee">
            <div class="col-12 col-md-10 col-lg-10">
                <div class="row py-3">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/mallareddy.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pailla Malla Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Pailla Malla Reddy</h6>
                                <div class="text-center pb-2">Founder</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/vijay.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vijayapal Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Vijayapal Reddy</h6>
                                <div class="text-center pb-2">Chair</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/haranath_policherla.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Haranath Policherla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Haranath Policherla</h6>
                                <div class="text-center pb-2">Co-Chair</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Mohan Reddy Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2">Council Chair</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="convention_executive_committee" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>
                                <div class="text-center pb-2">President</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>
                                <div class="text-center pb-2">Convener</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/vamshi_Reddy_2.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vamshi Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Vamshi Reddy</h6>
                                <div class="text-center pb-2">President-Elect</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Bharath_Madadi_12.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Bharath Madadi" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Bharath Madadi</h6>
                                <div class="text-center pb-2">Past President</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Suresh_venkannagari_3.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Suresh Reddy Venkannagari" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>
                                <div class="text-center pb-2">Executive Vice-President</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Srini-M_4.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Srinivasa Manapragada" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>
                                <div class="text-center pb-2">Genaral Secretary</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Pavan Ravva_6.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pavan Ravva" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Pavan Ravva</h6>
                                <div class="text-center pb-2">Treasurer</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/venkat-gaddam_5.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Gaddam" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Gaddam</h6>
                                <div class="text-center pb-2">Executive Director</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/kvmadam.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Kavitha Reddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Kavitha Reddy</h6>
                                <div class="text-center pb-2">Joint Secretary</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Naveen_Goli_10.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" width="150" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Naveen Goli</h6>
                                <div class="text-center pb-2">International Vice-President</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/venkat_aekka_9.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Aekka" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Aekka</h6>
                                <div class="text-center pb-2">National Co-Ordinator</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/harinder-tallapalli_8.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Harinder Tallapalli" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Harinder Tallapally</h6>
                                <div class="text-center pb-2">Joint Treasurer</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/narasimha_reddy_ln.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Narasimha Reddy Donthireddy" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>
                                <div class="text-center pb-2">Media & Communication Director</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/sole_madhavi_11.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Madhavi Soleti" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Madhavi Soleti</h6>
                                <div class="text-center pb-2">Ehtics Committee Co-Ordinator</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="user" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                                <div class="text-center pb-2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="convention-team">
                            <div>
                                <img src="images/Usha-Mannem.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Usha Mannam" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Usha Mannam</h6>
                                <div class="text-center pb-2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center" id="convention_regional_advisors" style="display: none;">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-lg-6 offset-lg-3">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                <div class="convention-team">
                                    <div>
                                        <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Santhosh Pathuri" />
                                    </div>
                                    <div class="pt-3">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">Santhosh Pathuri, NJ</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-6">
                                <div class="convention-team">
                                    <div>
                                        <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Manohar Kasagani" />
                                    </div>
                                    <div class="pt-3">
                                        <h6 class="text-violet text-center mb-1 font-weight-bold">Manohar Kasagani, Dallas</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Convention Team -->

<!-- Standing Committee -->

<section class="container-fluid pb80">
    <div class="mx-auto main-heading">
        <div>
            <span>Convention committees</span>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <ul class="list-unstyled text-center cc-horizontal-scroll">
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none convention-committee_active_tab_btn" target-id="#banquette">Banquette</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#media_n_communication">Media&nbsp;&&nbsp;Communication</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#piritual">Spiritual</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#budget_n_finance">Budget&nbsp;&&nbsp;Finance</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#overseas_coordination">Overseas&nbsp;Coordination</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#stage_n_av">Stage&nbsp;&&nbsp;AV</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#business">Business</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#political_forum">Political&nbsp;Forum</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#tta_star">TTA&nbsp;Star</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#cme">CME</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#programs_n_events">Programs&nbsp;and&nbsp;Events</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#transportation">Transportation</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#cultural">Cultural</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#reception">Reception</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#vendors_n_exhibits">Vendors&nbsp;&&nbsp;Exhibits</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#decoration">Decoration</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#registration">Registration</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#web_committee">Web&nbsp;committee</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#food">Food</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#safety_n_security">Safety&nbsp;&&nbsp;Security</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#women_committee">Women&nbsp;committee</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#hospitality">Hospitality</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#short_film">Short&nbsp;Film</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#youth_forum">Youth&nbsp;Forum</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#literacy">Literacy</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#social_media">Social&nbsp;Media</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#matrimonial">Matrimonial</a>
                        </li>
                        <li class="convention-committee-nav-item d-inline-flex">
                            <a class="convention-committee-nav-link text-dark fs18 pt10 pb10 text-decoration-none" target-id="#souvenir">Souvenir</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Malla Reddy Karra" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Malla Reddy Karra</h6>
                                <div>Audit Committee - Chair</div>
                                <div>Boston</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Vishnu Maddu" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Vishnu Maddu</h6>
                                <div>Audit Committee - Co-Chair</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Malla Reddy Karra" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Vijaypal Reddy</h6>
                                <div>Bylaws Committe - Chair</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Mohan Patalolla" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Mohan Patalolla</h6>
                                <div>Bylaws Committee - Co-Chair</div>
                                <div>New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Suresh Reddy Kokatam" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Suresh Reddy Kokatam</h6>
                                <div>Ethics Committee - Chair</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Venkat Nalluri Kokatam" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Venkat Nalluri</h6>
                                <div>Ethics Committee - Co-Chair</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Bhasker Pinna" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Bhasker Pinna</h6>
                                <div>Website Committee - Chair</div>
                                <div>Pennsylvania</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Nidheesh Raju Baskaruni" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Nidheesh Raju Baskaruni</h6>
                                <div>Website Committee - Co-Chair</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Venugopal Darur" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Venugopal Darur</h6>
                                <div>Website Committee - Member</div>
                                <div>TBU</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Ashok Chintakunta" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Ashok Chintakunta</h6>
                                <div>Cultural Committee - Chair</div>
                                <div>New York</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="Suman Mudamba" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Suman Mudamba</h6>
                                <div>Cultural Committee - Co-Chair</div>
                                <div>Delaware</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 my-3">
                        <div class="convention-committee-member-block">
                            <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-20" width="120" alt="" />
                            <div class="text-center pt-3">
                                <h6 class="text-violet fs16 mb-1">Deepthi Reddy</h6>
                                <div>Cultural Committee - Co-Chair</div>
                                <div>Atlanta</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 pt-3">
                        <div class="text-right">
                            <a href="standing_committe_members_details.php" class="text-decoration-none events-more-details-btn border-radius-20 fs16 btn-lg">View More <i class="fas fa-angle-right pl-2 see_more_arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Standing Committee -->

<!-- Donors -->

<section class="container-fluid pb80">
    <div class="mx-auto main-heading">
        <span>Convention Donors</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Harnath Policherla</h6>
                                <div class="text-center pb-2">Michigan</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Dr.Mohan Patalolla</h6>
                                <div class="text-center pb-2">New Jersey</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Malla Reddy Karra</h6>
                                <div class="text-center pb-2">Boston</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                        <div class="founding-member">
                            <div>
                                <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                            </div>
                            <div class="pt-3">
                                <h6 class="text-violet text-center mb-1 font-weight-bold">Vishnu Maddu</h6>
                                <div class="text-center pb-2">Atlanta</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Donors -->

<!-- Events -->

<section class="container-fluid pb80 bg-white">
    <div class="mx-auto main-heading">
        <span>Convention Program Schedule</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Breakfast and Matrimony</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Business conference, Women’s Con...</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Breakfast and Matrimony</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>27 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Business conference, Women’s Con...</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>28 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                        <div>
                            <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100" />
                        </div>
                        <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                            <div class="row">
                                <div class="col-12">
                                    <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                </div>
                                <div class="col-7 pt-2 my-auto">
                                    <div class=""><i class="far fa-calendar-alt pr-2"></i>29 May 2022</div>
                                </div>
                                <div class="col-5 px-2 pt-2 my-auto">
                                    <div class="text-right">
                                        <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Events -->

<!-- Media Partners -->

<section class="container-fluid pb80">
    <div class="mx-auto main-heading">
        <span>Media partners</span>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 py-3">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="images/tv9.png" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="images/ntv.png" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 my-3">
                        <div class="">
                            <img src="images/etv.png" class="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- End of Media Partners -->

<!-- End Of Content -->

<?php include 'footer.php';?>

<script>
    $(".convention-team-nav-link").click(function () {
        //active_tab
        $(".convention-team_active_tab_btn").removeClass("convention-team-nav-link");
        $(".convention-team_active_tab_btn").removeClass("convention-team_active_tab_btn");
        $(this).addClass("convention-team-nav-link");
        $(this).addClass("convention-team_active_tab_btn");

        $(".convention-team_active_tab").hide();
        $(".convention-team_active_tab").removeClass("convention-team_active_tab");
        $($(this).attr("target-id")).addClass("convention-team_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>

<script>
    $(".convention-committee-nav-link").click(function () {
        //active_tab
        $(".convention-committee_active_tab_btn").removeClass("convention-committee-nav-link");
        $(".convention-committee_active_tab_btn").removeClass("convention-committee_active_tab_btn");
        $(this).addClass("convention-committee-nav-link");
        $(this).addClass("convention-committee_active_tab_btn");

        $(".convention-committee_active_tab").hide();
        $(".convention-committee_active_tab").removeClass("convention-committee_active_tab");
        $($(this).attr("target-id")).addClass("convention-committee_active_tab");
        $($(this).attr("target-id")).show();
    });
</script>
