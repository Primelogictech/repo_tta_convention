<?php include 'header.php';?>     

    <section class="container-fluid mt80">
        <div class="mx-auto main-heading">
            <span>Events & Schedule</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>27 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>28 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>29 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Breakfast-and-matrimony.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Breakfast and Matrimony</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>27 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Business-and-womens_conference3.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Business conference, Women’s Con...</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>28 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4 my-3">
                            <div>
                                <img src="images/events/Laxmi-narsimhaswamy-kalyanam2.jpg" class="img-fluid border-radius-15 w-100">
                            </div>
                            <div class="col-12 py-2 shadow-small border-radius-10 event-block">
                                <div class="row">
                                    <div class="col-12">
                                        <div>Sri Laxmi Narasimha Swamy Kalyanam</div>
                                    </div>
                                    <div class="col-7 pt-2 my-auto">
                                        <div class="">
                                            <i class="far fa-calendar-alt pr-2"></i>29 August 2022
                                        </div>
                                    </div>
                                    <div class="col-5 px-2 pt-2 my-auto">
                                        <div class="text-right">
                                            <a href="page-under-constrution.php" class="btn events-more-details-btn border-radius-20 px-2 btn-sm">More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?> 