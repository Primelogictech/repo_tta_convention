<?php include 'header.php';?>    

    <section class="container-fluid mt80">
        <div class="mx-auto main-heading">
            <span>Executive Committee</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 pt-3 pb-5">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Mohan-Patalolla_1.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Mohan Reddy Patalolla</h6>
                                    <div class="text-center pb-2">PRESIDENT</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Srinivas_Ganagoni.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Dr.Mohan Patalolla" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Ganagoni</h6>
                                    <div class="text-center pb-2">Convener</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/vamshi_Reddy_2.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Vamshi Reddy" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Vamshi Reddy</h6>
                                    <div class="text-center pb-2">PRESIDENT-ELECT</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Bharath_Madadi_12.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Bharath Madadi" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Bharath Madadi</h6>
                                    <div class="text-center pb-2">PAST PRESIDENT</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Suresh_venkannagari_3.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Suresh Reddy Venkannagari" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Suresh Reddy Venkannagari</h6>
                                    <div class="text-center pb-2">EXECUTIVE VICE-PRESIDENT</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Srini-M_4.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Srinivasa Manapragada" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivasa Manapragada</h6>
                                    <div class="text-center pb-2">GENERAL SECRETARY</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Pavan Ravva_6.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Pavan Ravva" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Pavan Ravva</h6>
                                    <div class="text-center pb-2">TREASURER</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/venkat-gaddam_5.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Gaddam" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Gaddam</h6>
                                    <div class="text-center pb-2">EXECUTIVE DIRECTOR</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/kvmadam.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Kavitha Reddy" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Kavitha Reddy</h6>
                                    <div class="text-center pb-2">JOINT SECRETARY</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Naveen_Goli_10.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Naveen Goli</h6>
                                    <div class="text-center pb-2">INTERNATIONAL VICE-PRESIDENT</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/venkat_aekka_9.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Venkat Aekka" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Venkat Aekka</h6>
                                    <div class="text-center pb-2">NATIONAL CO-ORDINATOR</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/harinder-tallapalli_8.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Harinder Tallapalli" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Harinder Tallapally</h6>
                                    <div class="text-center pb-2">JOINT TREASURER</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/narasimha_reddy_ln.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Narasimha Reddy Donthireddy" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Narasimha Reddy Donthireddy(LN)</h6>
                                    <div class="text-center pb-2">MEDIA & COMMUNICATION DIRECTOR</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/sole_madhavi_11.png" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Madhavi Soleti" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Madhavi Soleti</h6>
                                    <div class="text-center pb-2">EHTICS COMMITTEE CO-ORDINATOR</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="user" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                                    <div class="text-center pb-2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                            <div class="executive-comite">
                                <div>
                                    <img src="images/Usha-Mannem.jpg" class="img-fluid border-radius-15 mx-auto d-block" width="150" alt="Usha Mannam" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Usha Mannam</h6>
                                    <div class="text-center pb-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?>