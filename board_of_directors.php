<?php include 'header.php';?>

<section class="container-fluid mt80">
    <div class="mx-auto main-heading">
        <div>
            <span>Board Of Directors</span>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-10 pt-3 pb-5">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/pra_zwb2qh.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Prasad Kunarapu</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/sharath-vemuganti.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Sharat Vemuganti</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Sahodhar-Peddireddy.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Sahodhar Peddireddy</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Usha-Mannem.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Usha Mannem</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/durga-prasad.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Durga Prasad</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/narendar-metuku.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Narender Metuku</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/satish-mekala.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Satish Reddy Mekala</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Srinivas-Guduru.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Guduru</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Bhaskar-Pinna.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Bhaskar Pinna</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/GangadharVuppala.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Gangadhar Vuppala</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Shiva-Reddy-Kolla.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Shiva Reddy Kolla</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Nishanth-Sirikonda.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Nishanth Sirikonda</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Ganesh-Veramaneni.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Ganesh Veramaneni</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Kiran Duddagi.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Kiran Duddagi</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Kiran-Guduru.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Kiran Guduru</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Mahohar-Rao-Bodke.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Manohar Rao Bodke</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/NikshipthaReddy-Koora.jpeg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Nikshiptha Reddy Koora</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/bindhulatha.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Bindu Latha Cheedella</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Gopi-Toka.jpg" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Gopi Tokala</h6>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3">
                        <div>
                            <img src="images/Karthik-Nimmala.png" class="img-fluid mx-auto d-block bod-img" width="150" alt="" />
                        </div>
                        <div class="pt-3 px-2">
                            <h6 class="text-violet text-center mb-1 font-weight-bold">Karthik Nimmala</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>