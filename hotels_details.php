<?php include 'header.php';?>
    
<section class="container-fluid my-3 my-lg-5">
    <div class="container shadow-small px-sm-30 py-4 p-md-4 p40">
        <div class="row">
            <div class="col-12">
                <h4 class="mb-4 text-violet">TTA Preferred Hotels</h4>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-skyblue">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-1.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Delta Hotel, Edison, NJ</h6>
                    <div class="text-center fs16">
                        <a href="https://g.page/DeltaWoodbridgeNJ?share" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>x
                    </div>
                </a>
            </div>
            <div class="col-12 col-md-6 col-lg-6 p20 mb-2 mb-lg-0 bg-light-orange">
                <a href="#" class="text-decoration-none">
                    <div>
                        <img src="images/hotel-2.png" class="img-fluid w-100 img-b p7" alt="">
                    </div>
                    <h6 class="text-danger pt-2 mb-1 text-center text-decoration-none">Shareton Edison, NJ</h6>
                    <div class="text-center fs16">
                        <a href="https://goo.gl/maps/fThqbKku73AWQ1QWA" class="text-decoration-none text-dark" target="_blank">Hotel Map
                        </a>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php';?>