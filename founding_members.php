<?php include 'header.php';?>

    <section class="container-fluid mt80">
        <div class="mx-auto main-heading">
            <span>Founding Members</span>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-10 py-3">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr. Harnath Policherla</h6>
                                    <div class="text-center pb-2">Michigan</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Dr.Mohan Patalolla</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Srinivas Anugu</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Vikram Reddy Jangam</h6>
                                    <div class="text-center pb-2">Dallas</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Santosh Pathuri</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Bharath Reddy Madaddi</h6>
                                    <div class="text-center pb-2">Atlanta</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Suresh Reddy Sadipiralla</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Mahender Reddy Musuku</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Goutam Reddy Goli</h6>
                                    <div class="text-center pb-2">Atlanta</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Anil Arrabelli</h6>
                                    <div class="text-center pb-2">California</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Ramesh Chandra</h6>
                                    <div class="text-center pb-2">New Jersey</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-3 py-3 my-1">
                            <div class="founding-member">
                                <div>
                                    <img src="images/malepic.jpeg" class="img-fluid mx-auto d-block border-radius-10" width="150" alt="" />
                                </div>
                                <div class="pt-3 px-2">
                                    <h6 class="text-violet text-center mb-1 font-weight-bold">Mahesh Adibhatla</h6>
                                    <div class="text-center pb-2">Dallas</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php';?>
