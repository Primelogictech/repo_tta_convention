<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Home Page</title>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Bootstrap CSS -->

        <link href="css/bootstrapcss.css" rel="stylesheet" />

        <!-- Fontawesome icons -->

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

        <!-- Animations -->

        <link href="css/Animations.css" rel="stylesheet" />

        <!-- Font family -->

        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" />

        <!--Datatables CSS-->

        <link href="css/datatables.css" rel="stylesheet" />

        <!-- AOS CSS -->

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />

        <!--Extra CSS-->

        <link href="css/custom.css" rel="stylesheet" />

        <link href="css/tta.css" rel="stylesheet" />

        <link href="css/sidebar.css" rel="stylesheet" />
    </head>

    <style type="text/css">
        .invitees-scroll {
            flex-wrap: nowrap;
            overflow-x: auto;
        }
        .invitees-scroll::-webkit-scrollbar {
            display: none;
        }
        .invitees-scroll {
            -ms-overflow-style: none; /* IE and Edge */
            scrollbar-width: none; /* Firefox */
        }
    </style>

    <body>
        <!--Goes Up-->

        <div class="topa">
            <i class="fas fa-arrow-up"></i>
        </div>

        <!-- Header -->
        <header>
            <div class="container-fluid top-layer-bg py-1 d-none d-lg-block">
                <div class="container">
                    <div class="row">
                        <div class="col-12 offset-md-1 col-md-10 offset-lg-3 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-5">
                                    <div class="text-center">
                                        <a href="#" class="text-white text-decoration-none fs15">Follow Us on: <i class="fab fa-facebook-f px-2"></i><i class="fab fa-twitter px-2"></i><i class="fab fa-youtube px-2"></i></a>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-7">
                                    <div class="text-center">
                                        <a href="#" class="text-white text-decoration-none helpline">Helpline : 1-866-TTA-SEVA (1-866-882-7382)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid logos_bg py-2 d-none d-lg-block">
                <div class="container mx-auto">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="text-center">
                                <a href="index.php" class="text-decoration-none">
                                    <div class="d-flex">
                                        <div class="my-auto pl35">
                                            <img src="images/tta_logo.png" class="logo img-fluid" />
                                        </div>
                                        <div class="my-auto pl80">
                                            <span class="pl20 my-auto text-danger">
                                                <strong class="web-name website-name">TELANGANA AMERICAN TELUGU ASSOCIATION</strong>
                                                <strong class="fs22 d-block website-name">తెలుగు కళల తోట | తెలంగాణ సేవల కోట </strong>
                                            </span>
                                        </div>
                                        <div class="my-auto pl100">
                                            <img src="images/tta_convention.png" class="logo img-fluid" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div data-toggle="sticky-onscroll">
                <div class="container-fluid bottom-layer-bg d-none d-lg-block">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div>
                                <nav class="navbar navbar-expand-lg navbar-light top-navbar py-0 justify-content-center d-none d-lg-block">
                                    <!-- Navbar links -->

                                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                                        <ul class="navbar-nav">
                                            <li class="nav-item">
                                                <a class="nav-link" href="index.php">Home</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                                    Registration
                                                </a>
                                                <div class="dropdown-menu submenu l-menu">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="awards_registrations.php" class="">Awards</a>
                                                                </li>
                                                                <li>
                                                                    <a href="cme_details.php" class="">CME</a>
                                                                </li>
                                                                <li>
                                                                    <a href="exhibits_reservation.php" class="">Exhibits</a>
                                                                </li>
                                                                <li>
                                                                    <a href="shathamanam_bhavathi_registration.php" class="">Shathamanam Bhavathi</a>
                                                                </li>
                                                                <li>
                                                                    <a href="youth_activities_registration.php" class="">Youth Activities</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="business_conference_details.php" class="">Business Conference</a>
                                                                </li>
                                                                <li>
                                                                    <a href="convention_details.php" class="">Convention</a>
                                                                </li>
                                                                <li>
                                                                    <a href="matrimony_registration.php" class="">Matrimony</a>
                                                                </li>
                                                                <li>
                                                                    <a href="tta_got_talent.php" class="">TTA Got Talent</a>
                                                                </li>
                                                                <li>
                                                                    <a href="lakshmi_narasimha_swami_kalyanam_details.php" class="">Lakshmi Narasimha Swami Kalyanam</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="business_plan_competition.php" class="">Business Plan Competition</a>
                                                                </li>
                                                                <li>
                                                                    <a href="cultural_registrations.php" class="">Cultural</a>
                                                                </li>
                                                                <li>
                                                                    <a href="miss_tta_mrs_tta_details.php" class="">Miss. TTA &amp; Mrs. TTA</a>
                                                                </li>
                                                                <li>
                                                                    <a href="women's_conference_details.php" class="">Women's Conference</a>
                                                                </li>
                                                                <li>
                                                                    <a href="souvenir_ads_registration.php" class="">Souvenir Registrations</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="hotels_details.php">Hotels</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="venue_details.php">Venue </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                                    Team
                                                </a>
                                                <div class="dropdown-menu submenu s-row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="advisory_council.php">Advisory Council</a>
                                                                </li>
                                                                <li>
                                                                    <a href="founding_members.php">Founding Members</a>
                                                                </li>
                                                                <li>
                                                                    <a href="executive_committee.php">Executive Committee</a>
                                                                </li>
                                                                <li>
                                                                    <a href="board_of_directors.php">Board Of Directors</a>
                                                                </li>
                                                                <li>
                                                                    <a href="standing_committe_members.php">Standing Committe Members</a>
                                                                </li>
                                                                <li>
                                                                    <a href="events_n_schedule.php">Events & Schedule</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                                    Committees
                                                </a>
                                                <div class="dropdown-menu submenu l-menu">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="banquet_night.php" class="">Banquette</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Budget & Finance</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Business</a>
                                                                </li>
                                                                <li>
                                                                    <a href="cme_details.php" class="">CME</a>
                                                                </li>
                                                                <li>
                                                                    <a href="cultural_registrations.php" class="">Cultural</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Decoration</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Food</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Hospitality</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Literacy</a>
                                                                </li>
                                                                <li>
                                                                    <a href="matrimony_registration.php" class="">Matrimonial</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="#" class="">Media & Communication</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Overseas Coordination</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Political Forum</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Programs and Events</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Reception</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Registration</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Safety & Security</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Short Film</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Social Media</a>
                                                                </li>
                                                                <li>
                                                                    <a href="souvenir_ads_registration.php" class="">Souvenir</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="#" class="">Spiritual</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Stage & AV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">TTA Star</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Transportation</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Vendors & Exhibits</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Web Committee</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Women Committee</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#" class="">Youth Forum</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="exhibits_reservation.php">Exhibits</a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="page-under-constrution.php">
                                                    Schedule
                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link" href="#" id="navbardrop" data-toggle="dropdown">
                                                    Donors
                                                </a>
                                                <div class="dropdown-menu submenu s-row">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <a href="page-under-constrution.php">Sapphire</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Diamond</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Platinum</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Gold</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Silver</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Bronze</a>
                                                                </li>
                                                                <li>
                                                                    <a href="page-under-constrution.php">Patron</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#">Contact&nbsp;Us</a>
                                            </li>
                                            <li class="nav-item active">
                                                <a class="nav-link lh25 text-white" href="signin.php">
                                                    Login
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 px-0">
                    <div class="header-border2">
                        <img src="images/tri.png" class="img-fluid d-none d-lg-block" alt="Thoranam" />
                    </div>
                </div>
            </div>
        </header>
        <header>
            <div class="container-fluid top-layer-bg py-1 d-block d-lg-none">
                <div class="container">
                    <div class="row">
                        <div class="col-12 offset-md-1 col-md-10 offset-lg-3 col-lg-6">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-5">
                                    <div class="text-center">
                                        <a href="#" class="text-white text-decoration-none fs15">Follow Us on: <i class="fab fa-facebook-f px-2"></i><i class="fab fa-twitter px-2"></i><i class="fab fa-youtube px-2"></i></a>
                                    </div>
                                </div>
                                <div class="col-12 col-md-7 col-lg-7">
                                    <div class="text-center">
                                        <a href="#" class="text-white text-decoration-none helpline">Helpline : 1-866-TTA-SEVA (1-866-882-7382)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid d-block d-lg-none">
                <div class="row">
                    <div class="col-12">
                        <div class="text-danger font-weight-bold">
                            <div class="web-name website-name text-center py-2">TELANGANA AMERICAN TELUGU ASSOCIATION</div>
                            <div class="web-name website-name text-center d-block pb-2">తెలుగు కళల తోట | తెలంగాణ సేవల కోట</div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="navbar-dark d-block d-lg-none bg-white py-2" data-toggle="sticky-onscroll">
            <div class="navbar">
                <!-- Brand -->

                <a class="navbar-brand py-0" href="index.php">
                    <img src="images/logo.png" alt="" border="0" width="60" class="img-fluid" />
                </a>

                <div class="nav-item">
                    <a class="nav-link px-1" href="#">
                        <span class="text-dark fs40 font-weight-bold" onclick="openNav()"><i class="fas fs30">&#xf039;</i></span>
                    </a>
                </div>
            </div>
        </nav>

        <div id="mySidenav" class="sidenav d-block d-lg-none">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <ul class="navbar-nav">
                <li class="sidemenu-nav-item sidemenu-active">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Home</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#"> <span>Registration</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                    <ul class="submenu d-none">
                        <li><a href="#">Awards</a></li>
                        <li><a href="business_conference_details.php">Business Conference</a></li>
                        <li><a href="business_plan_competition.php">Business Plan Competition</a></li>
                        <li><a href="cme_details.php">CME</a></li>
                        <li><a href="#">Convention</a></li>
                        <li><a href="#">Cultural</a></li>
                        <li><a href="exhibits_reservation.php">Exhibits</a></li>
                        <li><a href="matrimony_registration.php">Matrimony</a></li>
                        <li><a href="#">Miss. TTA &amp; Mrs. TTA</a></li>
                        <li><a href="shathamanam_bhavathi_registration.php">Shathamanam Bhavathi</a></li>
                        <li><a href="tta_got_talent.php">TTA Got Talent</a></li>
                        <li><a href="women's_conference_details.php">Women's Conference</a></li>
                        <li><a href="#">Youth Activities</a></li>
                        <li><a href="lakshmi_narasimha_swami_kalyanam_details.php">Lakshmi Narasimha Swami Kalyanam</a></li>
                        <li><a href="#">Souvenir Registrations</a></li>
                    </ul>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#"> <span>Team</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#"> <span>Speakers</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Exhibits</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item">
                    <a class="sidemnu-nav-link" href="#">
                        <span>Schedule</span>
                    </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#"> <span>Donors</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                </li>
                <li class="sidemenu-nav-item sidemenu-dropdown">
                    <a class="sidemnu-nav-link" href="#"> <span>Logistics</span><i class="fas fa-angle-right float-right pt4"></i> </a>
                </li>
            </ul>
        </div>
        <!-- <div class="container-fluid">
            <div class="row">
                <div class="col-12 px-0">
                    <div class="header-border2">
                        <img src="images/tri.png" class="img-fluid d-block" alt="Thoranam">
                    </div>
                </div>
            </div>
        </div> -->

        <!-- End Of Header -->
    