<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!-- Bootstrap CSS -->

        <link href="css/bootstrapcss.css" rel="stylesheet" />

        <!-- Fontawesome icons -->

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.2/css/all.css" />

        <!-- Animations -->

        <link href="css/Animations.css" rel="stylesheet" />

        <!-- Font family -->

        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300&display=swap" rel="stylesheet" />

        <!--Datatables CSS-->

        <link href="css/datatables.css" rel="stylesheet" />

        <!-- AOS CSS -->

        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

        <!--Extra CSS-->

        <link href="css/custom.css" rel="stylesheet" />

        <link href="css/telangana.css" rel="stylesheet" />

        <link href="css/sidebar.css" rel="stylesheet" />
    </head>

    <style type="text/css">
    	.vertical-height{
		    height: 100vh;
		    position: relative;
		}
		.vertical-center{
		    margin: 0;
		    position: absolute;
		    top: 50%;
		    -ms-transform: translateY(-50%) !important;
		    transform: translateY(-50%) !important;
		}
    </style>

    <body>

    	<div class="container vertical-height">
    		<div class="row">
    			<div class="col-12 vertical-center">
					<div>
    					<img src="images/under-construction.png" class="img-fluid mx-auto d-block" alt="">
    				</div>
    				<h3 class="text-center pt-5">This Page is Under Construction</h3>
    				<h5 class="text-center pt-1 text-muted">We're working on it</h5>
    			</div>
    		</div>
    	</div>

	<!-- All scripts -->
    <!-- jQuery library -->

    <script src="js/jquery.js"></script>

    <!-- Bootstrap JavaScript -->

    <script src="js/bootstrapjs.js"></script>

    <!--Popups js-->

    <script src="js/popperjs.js"></script>

    <!--Datatables js-->

    <script src="js/datatables.js"></script>

    <!-- Extra js -->

    <script src="js/extrajquery.js"></script>

    </body>
</html>